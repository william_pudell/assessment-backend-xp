<?php


namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ProductsModel extends DataLayer
{

    public function __construct()
    {
        parent::__construct("tb_produto", ["id_produto","nm_produto","ds_produto","nm_sku_produto","nu_quantidade","nu_preco"], "co_seq_produto", false);
    }
}