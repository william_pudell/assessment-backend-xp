<?php


namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class ProductCategoryModel extends DataLayer
{

    public function __construct()
    {
        parent::__construct("rl_produto_categoria", ["co_produto","co_categoria"], "co_seq_produto_categoria", false);
    }

}