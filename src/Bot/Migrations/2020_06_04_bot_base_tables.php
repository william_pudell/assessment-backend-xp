<?php

$sql = [
    ["CREATE TABLE IF NOT EXISTS `bot_migrations` (
        `co_seq_bot_migration` INT(11) NOT NULL AUTO_INCREMENT, 
        `nm_bot_migration` VARCHAR(80) NOT NULL, 
        `dt_created` DATETIME DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`co_seq_bot_migration`)
    ) ENGINE = InnoDB;    
    "],

    ["CREATE TABLE IF NOT EXISTS `tb_unique` (
        `id_unique` VARCHAR(80) NOT NULL,
        `nm_unique` VARCHAR(80) NULL,
        `dt_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id_unique`))
      ENGINE = InnoDB;
    "],

    ["CREATE TABLE IF NOT EXISTS `tb_produto` (
        `co_seq_produto` INT(11) NOT NULL AUTO_INCREMENT,
        `id_produto` VARCHAR(80) NOT NULL COMMENT 'ID único para o produto',
        `nm_produto` VARCHAR(80) NOT NULL COMMENT 'Nome do Produto',
        `ds_produto` VARCHAR(200) NOT NULL COMMENT 'Descrição do Produto',
        `nm_sku_produto` VARCHAR(80) NOT NULL COMMENT 'SKU do Produto',
        `nu_quantidade` INT(11) NOT NULL COMMENT 'Quantidade disponível do Produto',
        `nu_preco` DOUBLE(10,2) NOT NULL COMMENT 'Preço do Produto',
        `lk_image` VARCHAR(100) NULL COMMENT 'Link para a imagem', 
        `dt_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de inserção do registro',
        `dt_updated` DATETIME NULL COMMENT 'Data da última atualização do registro',
        `st_registro` INT(1) NOT NULL DEFAULT '1' COMMENT 'Situação do Registro 1: Ativo | 0: Inativo', 
        PRIMARY KEY(co_seq_produto)
    ) ENGINE = InnoDB;
    "],

    ["CREATE TABLE IF NOT EXISTS `tb_categoria` (
        `co_seq_categoria` INT(11) NOT NULL AUTO_INCREMENT,
        `id_categoria` VARCHAR(80) NOT NULL COMMENT 'ID único para a categoria',
        `nm_categoria` VARCHAR(80) NOT NULL COMMENT 'Nome da Categoria',
        `nm_codigo_categoria` VARCHAR(80) NOT NULL COMMENT 'Código da Categoria',
        `dt_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de inserção do registro',
        `dt_updated` DATETIME NULL COMMENT 'Data da última atualização do registro',
        `st_registro` INT(1) NOT NULL DEFAULT '1' COMMENT 'Situação do Registro 1: Ativo | 0: Inativo',
        PRIMARY KEY(co_seq_categoria)
    ) ENGINE = InnoDB;
    "],

    ["CREATE TABLE IF NOT EXISTS `rl_produto_categoria` (
        `co_seq_produto_categoria` INT(11) NOT NULL AUTO_INCREMENT,
        `co_produto` INT(11) NOT NULL COMMENT 'Código único do produto',
        `co_categoria` INT(11) NOT NULL COMMENT 'Código único da categoria',
        `dt_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de inserção do registro',
        `dt_updated` DATETIME NULL COMMENT 'Data da última atualização do registro',
        `st_registro` INT(1) NOT NULL DEFAULT '1' COMMENT 'Situação do Registro 1: Ativo | 0: Inativo',
        PRIMARY KEY(`co_seq_produto_categoria`),
        INDEX `idx_rl_produto_categoria_tb_produto` (`co_produto`),
        INDEX `idx_rl_produto_categoria_tb_categoria` (`co_categoria`),
        CONSTRAINT `fk_rl_produto_categoria_tb_produto`
            FOREIGN KEY(`co_produto`)
            REFERENCES `tb_produto` (`co_seq_produto`)
            ON DELETE NO ACTION 
            ON UPDATE NO ACTION,
        CONSTRAINT `fk_rl_produto_categoria_tb_categoria`
            FOREIGN KEY(`co_categoria`)
            REFERENCES `tb_categoria` (`co_seq_categoria`)
            ON DELETE NO ACTION 
            ON UPDATE NO ACTION
    ) ENGINE = InnoDB;
    "],

    ["CREATE TABLE IF NOT EXISTS `tb_log`(
        `co_seq_log` INT(11) NOT NULL AUTO_INCREMENT,
        `id_log` VARCHAR(80) NOT NULL COMMENT 'ID único para o log',
        `nm_origin` VARCHAR(80) NOT NULL COMMENT 'Nome da origem para referência de pesquisa',
        `js_log` TEXT NOT NULL COMMENT 'Texto de log de registros gerais, armazenado em formato JSON',        
        `dt_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de inserção do registro',
        `st_registro` INT(1) NOT NULL DEFAULT '1' COMMENT 'Situação do Registro 1: Ativo | 0: Inativo',
        PRIMARY KEY(`co_seq_log`)
    )"]

];