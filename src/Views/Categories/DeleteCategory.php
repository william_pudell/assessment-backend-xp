<main class="content">
    <h1 class="title new-item">Delete Category</h1>

    <form method="post">
        <div class="input-field">
            <label for="nm_categoria" class="label">Category Name</label>
            <input type="text" readonly disabled="disabled" name="nm_categoria" id="nm_categoria" value="<?= $categoria->nm_categoria; ?>" class="input-text" />

        </div>
        <div class="input-field">
            <label for="nm_codigo_categoria" class="label">Category Code</label>
            <input type="text" readonly disabled="disabled" name="nm_codigo_categoria" id="nm_codigo_categoria" value="<?= $categoria->nm_codigo_categoria; ?>" class="input-text" />

        </div>
        <div class="actions-form">
            <a href="/categories" class="action back">Back</a>
            <input class="btn-submit btn-action"  type="submit" value="Delete" />
        </div>
    </form>
</main>
<!-- Main Content -->
