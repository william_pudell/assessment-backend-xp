<?php


namespace Source\Controllers\Core;


class Session
{
    private $fields = [];
    protected $router;

    public function __construct($router = null)
    {
        if(\session_status() == PHP_SESSION_NONE){
            \session_start();
        }
        if($router != null){
            $this->router = $router;
        }
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getSessionValue($key)
    {

        if(in_array($key, $this->fields)) {
            return $_SESSION[$key];
        }
        return false;
    }

    public function checkSession()
    {
        foreach($this->fields as $field){
            if($_SESSION[$field] == ""){
                $this->cleanSession();
            }
        }

        return true;
    }

    public function isUserLogged()
    {
        foreach($this->fields as $field){
            if(isset($_SESSION[$field])){
                if($_SESSION[$field] == ""){
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public function setSession($data)
    {
        foreach($this->fields as $field){
            $_SESSION[$field] = $data[$field];
        }
    }

    public function cleanSession()
    {
        foreach($this->fields as $field){
            unset($_SESSION[$field]);
        }
        \session_destroy();
        \session_start();
        $this->router->redirect("landing");
    }

}