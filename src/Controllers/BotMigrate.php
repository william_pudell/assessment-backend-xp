<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Bot\Migrations;

class BotMigrate extends Master
{
    protected $migrations;
    public function __construct($router)
    {
        parent::__construct($router);
        $this->migrations = new Migrations($router);
    }

    public function init()
    {
        return $this->migrations->init();
    }

}