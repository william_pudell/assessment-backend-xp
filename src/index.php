<?php

// Required classes for the whole application boot
use Source\Controllers\Core\Handler;

// Require the composer autoload
require __DIR__ . "/../vendor/autoload.php";

// Set default timezone for the whole application
date_default_timezone_set('America/Sao_Paulo');

// Starting Handler for error class
$handler = new Handler();

// define the custom error handler for the whole application
set_error_handler(array($handler,'error_handler'));

// Require the Router file
require __DIR__ . "/router.php";