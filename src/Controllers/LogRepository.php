<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Models\LogModel;

class LogRepository extends Master
{

    protected $logModel;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->logModel = new LogModel();
    }

    public function insertLog(String $idLog, String $nm_origin, array $logData)
    {
        $log = $this->logModel;
        $log->id_log = $idLog;
        $log->nm_origin = $nm_origin;
        $log->dt_created = date("Y-m-d H:i:s");
        $log->js_log = json_encode($logData);
        $log->save();
        return;
    }

}