<?php


namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class LogModel extends DataLayer
{

    public function __construct()
    {
        parent::__construct("tb_log", ["id_log","nm_origin","js_log"], "co_seq_log", false);
    }

}