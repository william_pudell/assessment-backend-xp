<?php


namespace Source\Controllers\Core;

use StdClass;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    private $mail;

    private $data;

    private $error;

    public function __construct()
    {
        $this->mail = new PHPMailer();
        $this->data = new StdClass();

        $this->mail->isSMTP();
        $this->mail->isHTML(true);
        $this->mail->setLanguage("br");

        $this->mail->Host = SMTP_MAIL['host'];
        $this->mail->SMTPAuth = true;
        $this->mail->SMTPSecure = SMTP_MAIL['secure_type'];
        $this->mail->CharSet = SMTP_MAIL['charset'];
        $this->mail->Username = SMTP_MAIL['user'];
        $this->mail->Password = SMTP_MAIL['password'];
        $this->mail->Port = SMTP_MAIL['port'];

    }

    public function setData($to, $subject)
    {
        $this->mail->setFrom(SMTP_MAIL['from_mail'], SMTP_MAIL['from_name']);
        $this->mail->Subject = $subject;
        foreach($to as $key => $value){
            $this->mail->addAddress($value['recipient_email'], $value['recipient_name']);
        }
    }

    public function getMailConfig()
    {
        return $this->mail;
    }

    public function add(string $subject, string $body, string $recipient_name, string $recipient_email): Mail
    {
        $this->data->subject = $subject;
        $this->data->body = $body;
        $this->data->recipient_name = $recipient_name;
        $this->data->recipient_email = $recipient_email;
        return $this;
    }

    public function attach(string $filePath, string $fileName): Mail
    {
        $this->data->attach[$filePath] = $fileName;
        return $this;
    }

    public function send(string $from_name = SMTP_MAIL['from_name'], string $from_email = SMTP_MAIL['from_mail']): bool
    {
        try {

            $this->mail->Subject = $this->data->subject;
            $this->mail->Body = $this->data->body;
            $this->mail->addAddress($this->data->recipient_email, $this->data->recipient_name);
            $this->mail->setFrom($from_email, $from_name);

            if(!empty($this->data->attach)){
                foreach($this->data->attach as $path=>$name){
                    $this->mail->addAttachment($path, $name);
                }
            }

            $this->mail->send();
            return true;
        } catch (Exception $e){
            $this->error = $e;
            return false;
        }
    }

    public function error(): ?Exception
    {
        return $this->error;
    }
}