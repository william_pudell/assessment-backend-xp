<?php

namespace Source\Controllers\Core;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Handler
{

    public $logger;
    public $sendTo;
    public $subject;
    private $phpmailer;
    private $mail;

    public function __construct()
    {
        $this->logger = new Logger("web");
        $this->mail = new Mail;
        $this->sendTo = array(
            array("recipient_email"=>HANDLER_MAIL,"recipient_name"=>HANDLER_NAME),
        );
        $this->subject = "Error on ".SITE." at ".date("Y-m-d H:i:s");
        $this->mail->setData($this->sendTo,$this->subject);
        $this->phpmailer = new PHPMailerHandler($this->mail->getMailConfig(),Logger::ERROR);
        $this->logger->pushHandler(new StreamHandler(LOG_ROOT."log_".date("Y_m_d").".log",Logger::DEBUG));
        $this->logger->pushHandler($this->phpmailer);
        $this->logger->pushProcessor(function($record){
            $record["extra"]["HTTP_HOST"] = $_SERVER["HTTP_HOST"];
            $record["extra"]["REQUEST_URI"] = $_SERVER["REQUEST_URI"];
            $record["extra"]["REQUEST_METHOD"] = $_SERVER["REQUEST_METHOD"];
            $record["extra"]["HTTP_USER_AGENT"] = $_SERVER["HTTP_USER_AGENT"];
            return $record;
        });
    }

    public function error_handler($errno, $errstr, $errfile, $errline)
    {
        switch ($errno) {
            case E_USER_ERROR:
            case E_ERROR:
                $this->logger->error($errstr,["errno"=>$errno,"line"=>$errline,"file"=>$errfile]);
                break;
            case E_USER_WARNING:
            case E_WARNING:
                $this->logger->warning($errstr,["errno"=>$errno,"line"=>$errline,"file"=>$errfile]);
                break;
            case E_USER_NOTICE:
            case E_NOTICE:
                $this->logger->notice($errstr,["errno"=>$errno,"line"=>$errline,"file"=>$errfile]);
                break;
            default:
                $this->logger->debug($errstr,["errno"=>$errno,"line"=>$errline,"file"=>$errfile]);
                break;
        }
        return true;
    }

}