<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Models\CategoriesModel;

class Categories extends Master
{

    protected $categoriesModel;
    protected $category;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->categoriesModel = new CategoriesModel();
        $this->category = new CategoriesRepository($router);
    }

    public function init()
    {
        $listaCategorias = $this->categoriesList();
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Categories",
            "pageTitle"=>"Categories",
            "listaCategorias" => $listaCategorias,
            "addView"=>"Categories/ListCategories",
        ];
        echo $this->view->render("CategoriesView",$arrayVars);
    }

    public function new()
    {
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Add Category",
            "pageTitle"=>"Adicionar Categoria",
            "addView"=>"Categories/AddCategory",
        ];
        echo $this->view->render("CategoriesView",$arrayVars);
    }

    public function edit($data)
    {
        $categoria = $this->category->getCategory($data['id_categoria']);
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Edit Category",
            "pageTitle"=>"Editar Categoria",
            "addView"=>"Categories/EditCategory",
            "categoria"=>$categoria,
        ];
        echo $this->view->render("CategoriesView",$arrayVars);
    }

    public function delete($data)
    {
        $categoria = $this->category->getCategory($data['id_categoria']);
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Delete Category",
            "pageTitle"=>"Deletar Categoria",
            "addView"=>"Categories/DeleteCategory",
            "categoria"=>$categoria,
        ];
        echo $this->view->render("CategoriesView",$arrayVars);
    }

    public function categoriesList()
    {
        $list = array();
        $categories = $this->categoriesModel->find("st_registro=1")->fetch(true);
        foreach($categories as $category){
            $list[] = array(
                "co_seq_categoria"=>$category->co_seq_categoria,
                "nm_categoria"=>$category->nm_categoria,
                "nm_codigo_categoria"=>$category->nm_codigo_categoria,
                "id_categoria"=>$category->id_categoria
            );
        }

        return $list;
    }


}