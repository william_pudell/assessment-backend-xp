<ul class="product-list">
    <?php foreach($listaProdutos as $produto): ?>
        <li>
            <?php if(!!$produto['lk_imagem']): ?>
                <div class="product-image">
                    <?php if(isset($produto['lk_produto'])): ?>
                        <a href="<?= $produto['lk_produto']; ?>" title="<?= $produto['nm_produto']; ?>">
                    <?php endif; ?>
                    <img src="<?= $produto['lk_imagem'] ?>" layout="responsive" width="164" height="145" alt="<?= $produto['nm_produto']; ?>" />
                    <?php if(isset($produto['lk_produto'])): ?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <div class="product-info">
                <div class="product-name"><span><?= $produto['nm_produto']; ?></span></div>
                <div class="product-price"><span class="special-price"><?= $produto['nu_disponivel']; ?></span> <span><?= $produto['nu_preco']; ?></span></div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
