# Instruções   
Para instalação da aplicação é necessário executar algumas etapas, sendo a primeira delas o composer       

# Instalação   
Ao clonar o projeto é necessário executar o composer para a instalação dos pacotes.   

## Banco de Dados   
Deve-se criar um usuário com permissão completa ao banco de dados que será utilizado para a execução da aplicação      
Os dados de acesso devem ser incluidos no arquivo ```config.ini``` nos seguintes parametros:   
- db_name - Nome do Banco de Dados (Schema)   
- db_user - Usuário de acesso ao Banco de Dados   
- db_pass - Senha de acesso ao Banco de Dados   

## Configuração   
Dentro do projeto, na pasta src existe um arquivo chamado ```config.ini``` nele estão os principais parametros para a
execução da aplicação, como dados de accesso ao banco de dados, configuração para envio de e-mail,
e-mail do(s) reponsavel(is) pela correção de erros na aplicação, segredo para utilizar no jwt.   
Porém para este teste apenas os dados de banco de dados serão utilizados, uma vez que api e e-mail não estão
sendo utilizados, mas podem ser configurados caso queira   
 
 
# Pacotes utilizados   
No composer temos os pacotes necessários para a execução da aplicação, abaixo da lista uma breve explicação do uso de cada pacote, são eles:   
- coffeecode/router   
- coffeecode/datalayer   
- league/plates   
- monolog/monolog   
- phpmailer/phpmailer    
- firebase/jwt    

## coffeecode/router   
Pacote utilizado para gerenciar as url amigaveis na aplicação   

## coffeecode/datalayer   
Pacote que intermedia a conexão com o banco de dados, no caso mysql 5.7   

## league/plates   
Pacote responsável pelas views da aplicação, utilizado para facilitar a reusabilidade de html sem necessidade de duplicar arquivos   

## monolog/monolog   
Pacote responsável por gerar logs de erros   

## phpmailer/phpmailer    
Pacote responsável pelo envio de e-mails da aplicação (utilizado no contexto de erros)   

## firebase/jwt    
Pacote responsável por gerar tokens de acesso e garantir a validade de tokens préviamente gerados   

