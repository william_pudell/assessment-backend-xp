<!-- Main Content -->
<main class="content">
    <div class="header-list-page">
        <h1 class="title">Categories</h1>
        <a href="/categories/new" class="btn-action">Add new Category</a>
    </div>
    <?php if(!!$listaCategorias): ?>
        <table class="data-grid">
            <tr class="data-row">
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Name</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Code</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Actions</span>
                </th>
            </tr>
            <?php foreach($listaCategorias as $categoria): ?>
                <tr class="data-row">
                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $categoria['nm_categoria']; ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $categoria['nm_codigo_categoria']; ?></span>
                    </td>

                    <td class="data-grid-td">
                        <div class="actions">
                            <div class="action edit"><a href="/categories/edit/<?= $categoria['id_categoria'] ?>">Edit</a></div>
                            <div class="action delete"><a href="/categories/delete/<?= $categoria['id_categoria'] ?>">Delete</a></div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php else: ?>
        Nenhuma Categoria Cadastrada
    <?php endif; ?>
</main>
<!-- Main Content -->