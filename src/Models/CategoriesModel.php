<?php


namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class CategoriesModel extends DataLayer
{
    public function __construct()
    {
        parent::__construct("tb_categoria", ["id_categoria","nm_categoria","nm_codigo_categoria"], "co_seq_categoria", false);
    }

}