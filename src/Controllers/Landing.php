<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Controllers\Products;

class Landing extends Master
{
    protected $products;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->products = new Products($router);
    }

    public function init()
    {
        $listaProdutos = $this->products->listProducts();
        $countProducts = count($listaProdutos);
        $countProducts .= (count($listaProdutos) > 1) ? " products ": " product ";
        $arrayVars = [
            "pageTitle"=>"Dashboard",
            "listaProdutos"=>$listaProdutos,
            "countProducts"=>$countProducts,
        ];

        echo $this->view->render("LandingView",$arrayVars);
    }

}