<!-- Main Content -->
<main class="content">
    <div class="header-list-page">
        <h1 class="title">Products</h1>
        <a href="/products/new" class="btn-action">Add new Product</a>
    </div>
    <?php if(!!$products): ?>
        <table class="data-grid">
            <tr class="data-row">
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Name</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">SKU</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Price</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Quantity</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Categories</span>
                </th>

                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Actions</span>
                </th>
            </tr>
            <?php foreach($products as $product): ?>
                <tr class="data-row">
                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $product['nm_produto']; ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $product['nm_sku_produto']; ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">R$ <?= $product['nu_preco']; ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $product['nu_quantidade']; ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">
                            <?php foreach ($product['categorias'] as $categoria):
                                echo $categoria['nm_categoria']."<br/>";
                            endforeach; ?>
                        </span>
                    </td>

                    <td class="data-grid-td">
                        <div class="actions">
                            <div class="action edit"><a href="/products/edit/<?= $product['id_produto']; ?>">Edit</a></div>
                            <div class="action delete"><a href="/products/delete/<?= $product['id_produto']; ?>">Delete</a></div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php else: ?>
        Nenhum produto disponível
    <?php endif; ?>
</main>
<!-- Main Content -->