<?php

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER['HTTP_HOST'];

$ini = parse_ini_file('config.ini');

define("URL_BASE",$url);
define("ROOT",$url);

define("SITE",  $ini['site_name']); //Site Name

define("HANDLER_NAME",$ini['handler_name']);
define("HANDLER_MAIL",$ini['handler_mail']);

define("ROOT_NAMESPACE","Source");
define("ROOT_MODEL", __DIR__."/Models/");
define("ROOT_CONTROLLER", __DIR__."/Controllers/");
define("LANG_DEFAULT", [
    'co_lang'=>1,
    'nm_lang'=>'pt-br'
]);

define("ROOT_VIEW", __DIR__ . "/Views/");
define("ROOT_MIGRATIONS",__DIR__."/Bot/Migrations/");
define("LOG_ROOT",__DIR__."/Logs/");


define("DATA_LAYER_CONFIG", [
    "driver" => "mysql",
    "host" => "localhost",
    "port" => "3306",
    "dbname" => $ini['db_name'],
    "username" => $ini['db_user'],
    "passwd" => $ini['db_pass'],
    "options" => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_CASE => PDO::CASE_NATURAL
    ]
]);

define("SMTP_MAIL",[
    "host" => $ini['mail_host'],
    "port" => $ini['mail_port'],
    "user" => $ini['mail_user'],
    "password" => $ini['mail_pass'],
    "from_name" => $ini['from_name'],
    "from_mail" => $ini['from_mail'],
    "secure_type" => "ssl",
    "charset" => "utf-8"
]);