<?php


namespace Source\Controllers\Core;

use CoffeeCode\DataLayer\Connect;

class UniqueGenerator
{

    protected $characters;
    protected $size;
    protected $connect;

    public function __construct(int $size = 11)
    {
        $this->characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_';
        $this->size = $size;
        $this->connect = Connect::getInstance();
    }

    public function init(string $nm_unique = null)
    {
        return $this->validate($nm_unique);
    }

    protected function validate(string $nm_unique)
    {
        $result = $this->generator();
        $rs = $this->connect->query("SELECT COUNT(*) as countUnique FROM tb_unique WHERE id_unique = '{$result}'")->fetchAll();
        if(!!$rs[0]->countUnique){
            $result = $this->validate();
        } else {
            $this->connect->query("INSERT INTO tb_unique (id_unique, nm_unique) VALUES ('{$result}','{$nm_unique}')");
        }
        return $result;
    }

    protected function generator()
    {
        $result = '';
        for ($i = 0; $i < $this->size; $i++) {
            $result .= $this->characters[mt_rand(0, strlen($this->characters)-1)];
        }
        return $result;
    }
}