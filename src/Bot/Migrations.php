<?php


namespace Source\Bot;

use Exception;
use PDOException;
use CoffeeCode\DataLayer\Connect;
use Source\Controllers\Core\Handler;
use Source\Controllers\Core\Master;

class Migrations extends Master
{

    protected $dir;
    protected $sql;
    protected $connect;
    protected $dbname;
    protected $runMigration;
    public $files;
    public $mustAuth;
    public $haveMigration;
    public $firstMigration;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->dir = ROOT_MIGRATIONS;
        $this->connect = Connect::getInstance();
        $this->dbname = DATA_LAYER_CONFIG["dbname"];
        $this->mustAuth = $this->checkMigrationTable();
        $this->haveMigration = true;
        $this->runMigration = false;
        $this->firstMigration = true;
    }

    public function init()
    {
        $this->getFiles();
        $this->importFile();
        if($this->mustAuth){
            $this->runMigration = $this->session->checkSessionMigration();
            if(!$this->runMigration){
                $this->session->cleanSession();
            }
            $this->checkIfHaveMigration();
        } else {
            $this->runMigration = true;
        }

        if($this->haveMigration){
            if($this->runMigration){
                foreach($this->sql as $filename => $queries){
                    if($this->migrateFile($filename, $queries)){
                        $retorno['migrations'][] = array(
                            'filename' => $filename
                        );
                    }
                }
                $retorno['co_mensagem'] = 0;
                $retorno['tx_mensagem'] = 'migrations executadas com sucesso';
            } else {
                $retorno['co_mensagem'] = 1;
                $retorno['tx_mensagem'] = 'Você deve estar autenticado para exeuctar as migrations';
            }
        } else {
            $retorno['co_mensagem'] = 0;
            $retorno['tx_mensagem'] = 'Nenhuma migration para executar';
        }

        $this->returnJson($retorno);
        return;
    }

    protected function getFiles()
    {
        $this->files = glob($this->dir."*.php");
        foreach($this->files as $key => $file){
            $filename = explode("/",$file);
            if(end($filename) == "Migrations.php"){
                unset($this->files[$key]);
            }
        }
    }

    protected function importFile()
    {
        $sql = "";
        foreach($this->files as $file){
            $filename = explode("/",$file);
            $extension = explode(".",end($filename));
            if(end($extension) == "sql"){
                $sql = \file_get_contents($file);
                $temp = explode(";".PHP_EOL,$sql);
                $sql = array();
                foreach($temp as $key => $val){
                    if($val != ""){
                        $temp[$key] .= ";";
                        $sql[][] = $temp[$key];
                    }
                    unset($temp[$key]);
                }
                $this->addSql(end($filename), $sql);
            } else {
                include_once $file;
                $this->addSql(end($filename), $sql);
            }
        }
        ksort($this->sql);
    }

    protected function addSql($file, $sql)
    {
        $this->sql[$file] = $sql;
    }

    protected function checkMigrationTable()
    {
        $sql = "SHOW TABLES FROM {$this->dbname} LIKE 'bot_migrations'";
        $rs = $this->connect->query($sql)->fetch(true);
        if(empty($rs)){
            return false;
        } else {
            return true;
        }
    }

    protected function migrateFile($filename, $queries)
    {
        $filename = explode(".",$filename)[0];
        $rs = "";
        if($this->firstMigration){
            $this->firstMigration = $this->checkMigrationTable();
            if(!$this->firstMigration){
                return $this->tryMigrate($filename, $queries);
            }
        }
        if($this->checkIfFileIsMigrated($filename)){
            return $this->tryMigrate($filename, $queries);
        }
        return false;
    }

    protected function tryMigrate($filename, $queries)
    {
        foreach($queries as $query){
            try {
                $rs = $this->connect->query($query[0]);
            } catch (PDOException $ePDO){
                (new Handler())->error_handler(E_ERROR, $ePDO."->(".$query[0].")",$filename,0);
                $this->returnError($ePDO);
                die();
            } catch (Exception $e){
                (new Handler())->error_handler(E_ERROR, $e."->(".$query[0].")",$filename,0);
                $this->returnError($e);
                die();
            }
        }
        $this->registerMigratedFile($filename);
        return true;
    }

    protected function checkMustMigrateFile($file)
    {
        $sql = "SHOW TABLES FROM {$this->dbname} LIKE '{$file}'";
        $rs = $this->connect->query($sql)->fetch(true);
        if(empty($rs)){
            return true;
        } else {
            return false;
        }
    }

    protected function checkIfFileIsMigrated($file)
    {

        $sql = "SELECT COUNT(*) as qtd FROM bot_migrations WHERE nm_bot_migration = '{$file}'";
        $rs = $this->connect->query($sql)->fetch(true);
        if($rs->qtd == 0){
            return true;
        } else {
            return false;
        }
    }

    protected function registerMigratedFile($file)
    {
        $sql = "INSERT INTO bot_migrations (nm_bot_migration) VALUES ('{$file}')";
        $rs = $this->connect->query($sql);
    }

    protected function checkIfHaveMigration()
    {
        $count = 0;
        foreach($this->sql as $file => $queries){
            $filename = explode(".",$file)[0];
            if($this->checkIfFileIsMigrated($filename)){
                $count++;
            }
        }
        $this->haveMigration = (boolean) $count;
    }

    protected function returnError($message){
        $this->returnJson($message);
        die();
    }

}