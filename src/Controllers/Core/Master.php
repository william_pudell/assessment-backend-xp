<?php


namespace Source\Controllers\Core;

use League\Plates\Engine;
use CoffeeCode\DataLayer\Connect;

class Master
{
    protected $connect;
    protected $view;
    protected $router;
    protected $session;

    public function __construct($router)
    {
        $this->router = $router;
        $this->connect = Connect::getInstance();
        $this->session = new Session($router);
        $this->view = Engine::create(ROOT_VIEW,"php");
        $this->view->addData(["router"=>$router]);
    }

    /**
     * Method for debug purposes
     * @param $variable
     */
    function dumpData($variable)
    {
        print_r("<pre>");
        print_r($variable);
        print_r("</pre>");
        die();
    }

    /**
     * Return a json formatted
     * @param array $data
     * @echo json_encode $data
     */
    function returnJson($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    /**
     * Return Unauthorized
     * Changes the page header and return a json
     * calling returnJson method
     */
    function return401()
    {
        header("HTTP/1.0 401 Unauthorized");
        $return['code'] = 401;
        $return['message'] = "Unauthorized";
        returnJson($return);
    }

    /**
     * General function to retrieve a full qualified URL from the project to the view (plates) layer
     * @param string $uri
     * @return string
     */
    function url($uri = null) {
        if($uri) {
            return ROOT . "/{$uri}";
        }
        return ROOT;
    }

    /**
     * Function to redirect to another URL inside the application
     * @param $route
     * @return mixed
     */
    function redirect($route)
    {
        return $this->router->redirect($route);
    }

}