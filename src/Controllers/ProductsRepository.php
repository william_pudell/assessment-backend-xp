<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Controllers\Core\UniqueGenerator;
use Source\Models\ProductCategoryModel;
use Source\Models\ProductsModel;
use Source\Controllers\LogRepository;

class ProductsRepository extends Master
{

    protected $uniqueGenerator;
    protected $product;
    protected $productCategory;
    protected $logRepository;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->uniqueGenerator = new UniqueGenerator();
        $this->product = new ProductsModel();
        $this->productCategory = new ProductCategoryModel();
        $this->logRepository = new LogRepository($router);
    }

    public function getProduct($id_produto)
    {
        return $this->product->find("id_produto = :id_produto","id_produto={$id_produto}")->fetch(true)[0];
    }

    public function getProductId($co_produto)
    {
        return $this->product->findById($co_produto);
    }

    public function add($data)
    {

        $imagem = $_FILES['lk_imagem'];
        $upload = $imagem['tmp_name'];
        $expName = explode(".",$imagem['name']);
        $ext = end($expName);
        $lk_imagem = "Images/product/".$this->uniqueGenerator->init("Image for product: {$data['nm_produto']}").".".$ext;
        move_uploaded_file($upload,$lk_imagem);

        $logData["action"] = "product/add";
        $logData["dados"] = $data;
        $logData["imagem"] = $imagem;
        $logData["lk_imagem"] = $lk_imagem;
        $log["nm_origin"] = "ProductsRespository";
        $log["id_log"] = $this->uniqueGenerator->init("Log for insert product: {$data['nm_produto']}");

        $data['id_produto'] = $this->uniqueGenerator->init("Product: {$data['nm_produto']}");
        $data['lk_imagem'] = $lk_imagem;

        $categories = $data['co_categoria'];
        unset($data['co_categoria']);

        $data['nu_preco'] = str_replace(",",".",$data['nu_preco']);

        $product = $this->product;
        foreach ($data as $k => $v){
            $product->$k = $v;
        }

        $product->save();

        foreach($categories as $k=>$v){
            $productCategory = $this->productCategory;
            $productCategory->co_produto = $product->co_seq_produto;
            $productCategory->co_categoria = $v;
            $productCategory->save();
        }

        $this->logRepository->insertLog($log['id_log'],$log["nm_origin"],$logData);

        return $this->redirect("productsList");
    }

    public function update($data)
    {

        $logData["action"] = "product/update";
        $logData["dados"] = $data;

        $categories = $data['co_categoria'];
        unset($data['co_categoria']);

        $data['nu_preco'] = str_replace(",",".",$data['nu_preco']);
        $data['dt_updated'] = date("Y-m-d H:i:s");

        $product = $this->getProduct($data['id_produto']);
        foreach ($data as $k => $v){
            $product->$k = $v;
        }

        $product->save();

        $log["nm_origin"] = "ProductsRespository";
        $log["id_log"] = $this->uniqueGenerator->init("Log for update product: {$product->nm_produto}");

        $productCategories = $this->productCategory->find("co_produto = :co_produto","co_produto={$product->co_seq_produto}")->fetch(true);
        foreach ($productCategories as $productCategory){
            $productCategory->dt_updated = date("Y-m-d H:i:s");
            $productCategory->st_registro = 0;
            $productCategory->save();
        }

        foreach($categories as $k=>$v){
            $productCategory = $this->productCategory->find("co_produto = :co_produto AND co_categoria = :co_categoria","co_produto={$product->co_seq_produto}&co_categoria={$v}")->fetch(true);
            if($productCategory) {
                $productCategory[0]->dt_updated = date("Y-m-d H:i:s");
                $productCategory[0]->st_registro = 1;
                $productCategory[0]->save();
            } else {
                $productCategory = $this->productCategory;
                $productCategory->co_produto = $product->co_seq_produto;
                $productCategory->co_categoria = $v;
                $productCategory->save();
            }
        }

        $this->logRepository->insertLog($log['id_log'],$log["nm_origin"],$logData);

        return $this->redirect("productsList");
    }

    public function delete($data)
    {

        $logData["action"] = "product/delete";
        $logData["dados"] = $data;

        $categories = $data['co_categoria'];
        unset($data['co_categoria']);

        $data['dt_updated'] = date("Y-m-d H:i:s");
        $data['st_registro'] = 0;

        $product = $this->getProduct($data['id_produto']);
        foreach ($data as $k => $v){
            $product->$k = $v;
        }

        $product->save();

        $log["nm_origin"] = "ProductsRespository";
        $log["id_log"] = $this->uniqueGenerator->init("Log for delete product: {$product->nm_produto}");

        $productCategories = $this->productCategory->find("co_produto = :co_produto","co_produto={$product->co_seq_produto}")->fetch(true);
        foreach ($productCategories as $productCategory){
            $productCategory->dt_updated = date("Y-m-d H:i:s");
            $productCategory->st_registro = 0;
            $productCategory->save();
        }

        $this->logRepository->insertLog($log['id_log'],$log["nm_origin"],$logData);

        return $this->redirect("productsList");
    }
}