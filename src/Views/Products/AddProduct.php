<!-- Main Content -->
<main class="content">
    <h1 class="title new-item">New Product</h1>

    <form method="post" action="/products/add" enctype="multipart/form-data">
        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="nm_sku_produto" name="nm_sku_produto" class="input-text" />
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="nm_produto" name="nm_produto" class="input-text" />
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="nu_preco" name="nu_preco" class="input-text" />
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="nu_quantidade" name="nu_quantidade" class="input-text" />
        </div>
        <div class="input-field">
            <label for="category" class="label">Categories</label>
            <select multiple id="co_categoria[]" name="co_categoria[]" class="input-text">
                <?php foreach($categorias as $categoria): ?>
                    <option value="<?= $categoria['co_seq_categoria']; ?>"><?= $categoria['nm_codigo_categoria'] ?> - <?= $categoria['nm_categoria']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="ds_produto" name="ds_produto" class="input-text"></textarea>
        </div>
        <div class="input-field">
            <label for="lk_imagem" class="label">Product Image</label>
            <input type="file" name="lk_imagem" accept="image/*" class="input-text">
        </div>
        <div class="actions-form">
            <a href="/products" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save Product" />
        </div>

    </form>
</main>
<!-- Main Content -->