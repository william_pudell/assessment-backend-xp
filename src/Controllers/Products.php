<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Models\ProductCategoryModel;
use Source\Models\ProductsModel;

class Products extends Master
{

    protected $productModel;
    protected $categories;
    protected $productCategory;
    protected $productRepository;
    protected $categoryRepository;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->productModel = new ProductsModel();
        $this->categories = new Categories($router);
        $this->productCategory = new ProductCategoryModel();
        $this->productRepository = new ProductsRepository($router);
        $this->categoryRepository = new CategoriesRepository($router);
    }

    public function init()
    {
        $products = $this->listProducts();
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Products",
            "pageTitle"=>"Adicionar Produto",
            "addView"=>"Products/ListProducts",
            "products"=>$products,
        ];
        echo $this->view->render("ProductsView",$arrayVars);
    }

    public function new()
    {
        $categorias = $this->categories->categoriesList();
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Add Product",
            "pageTitle"=>"Adicionar Produto",
            "addView"=>"Products/AddProduct",
            "categorias"=>$categorias,
        ];
        echo $this->view->render("ProductsView",$arrayVars);
    }

    public function edit($data)
    {
        $produto = $this->productRepository->getProduct($data['id_produto']);
        $produtoCategorias = $this->productCategory->find("co_produto = :co_produto","co_produto=$produto->co_seq_produto")->fetch(true);
        $categorias = $this->categories->categoriesList();
        foreach ($categorias as $key => $categoria){
            foreach ($produtoCategorias as $produtoCategoria){
                if($produtoCategoria->co_categoria == $categoria['co_seq_categoria']){
                    $categorias[$key]['select'] = "selected";
                }
            }
        }
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Edit Product",
            "pageTitle"=>"Editar Produto",
            "addView"=>"Products/EditProduct",
            "produto"=>$produto,
            "produtoCategorias"=>$produtoCategorias,
            "categorias"=>$categorias,
        ];

        echo $this->view->render("ProductsView",$arrayVars);
    }

    public function delete($data)
    {
        $produto = $this->productRepository->getProduct($data['id_produto']);
        $produtoCategorias = $this->productCategory->find("co_produto = :co_produto","co_produto=$produto->co_seq_produto")->fetch(true);
        $categorias = $this->categories->categoriesList();
        foreach ($categorias as $key => $categoria){
            foreach ($produtoCategorias as $produtoCategoria){
                if($produtoCategoria->co_categoria == $categoria['co_seq_categoria']){
                    $categorias[$key]['select'] = "selected";
                }
            }
        }
        $arrayVars = [
            "htmlTitle"=>"Webjump | Backend Test | Delete Product",
            "pageTitle"=>"Deletar Produto",
            "addView"=>"Products/DeleteProduct",
            "produto"=>$produto,
            "produtoCategorias"=>$produtoCategorias,
            "categorias"=>$categorias,
        ];

        echo $this->view->render("ProductsView",$arrayVars);
    }


    public function listProducts()
    {
        $list = array();
        $products = $this->productModel->find("st_registro=1")->fetch(true);
        foreach ($products as $product){
            $categories = $this->productCategory->find('co_produto = :co_produto AND st_registro = 1',"co_produto={$product->co_seq_produto}")->fetch(true);
            $categorias = array();
            foreach ($categories as $category) {
                $categoria = $this->categoryRepository->getCategoryId($category->co_categoria);
                $categorias[] = array(
                    "co_categoria"=>$categoria->co_seq_categoria,
                    "id_categoria"=>$categoria->id_categoria,
                    "nm_categoria"=>$categoria->nm_categoria,
                    "nm_codigo_categoria"=>$categoria->nm_codigo_categoria,
                );
            }
            $lk_imagem = ($product->lk_imagem != "") ? $this->url($product->lk_imagem) : "";
            $nu_preco = number_format($product->nu_preco,"2",","," ");
            $list[] = array(
                "co_seq_produto"=>$product->co_seq_produto,
                "id_produto"=>$product->id_produto,
                "nm_produto"=>$product->nm_produto,
                "ds_produto"=>$product->ds_produto,
                "nm_sku_produto"=>$product->nm_sku_produto,
                "nu_quantidade"=>$product->nu_quantidade,
                "nu_preco"=>$nu_preco,
                "lk_imagem"=>$lk_imagem,
                "categorias"=>$categorias,
            );
        }

        return $list;
    }

}