<?php


namespace Source\Controllers;

use Source\Controllers\Core\Master;
use Source\Controllers\Core\UniqueGenerator;
use Source\Models\CategoriesModel;
use Source\Controllers\LogRepository;

class CategoriesRepository extends Master
{

    protected $uniqueGenerator;
    protected $category;
    protected $logRepository;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->uniqueGenerator = new UniqueGenerator();
        $this->category = new CategoriesModel();
        $this->logRepository = new LogRepository($router);
    }

    public function getCategory($id_categoria)
    {
        return $this->category->find("id_categoria = :id_categoria","id_categoria={$id_categoria}")->fetch(true)[0];
    }

    public function getCategoryId($co_categoria)
    {
        return $this->category->findById($co_categoria);
    }

    public function add($data)
    {
        $logData["action"] = "categories/add";
        $logData["dados"] = $data;
        $log["nm_origin"] = "CategoriesRespository";
        $log["id_log"] = $this->uniqueGenerator->init("Log for add category: {$data['nm_categoria']}");

        $data['id_categoria'] = $this->uniqueGenerator->init("Category: {$data['nm_categoria']}");
        $category = $this->category;
        foreach($data as $k => $v){
            $category->$k = $v;
        }
        $category->save();

        $this->logRepository->insertLog($log['id_log'],$log["nm_origin"],$logData);

        return $this->redirect("categoriesList");

    }

    public function update($data)
    {
        $logData["action"] = "categories/update";
        $logData["dados"] = $data;

        $category = $this->getCategory($data['id_categoria']);

        $data['dt_updated'] = date("Y-m-d H:i:s");
        foreach($data as $k => $v){
            $category->$k = $v;
        }
        $category->save();

        $log["nm_origin"] = "CategoriesRespository";
        $log["id_log"] = $this->uniqueGenerator->init("Log for update category: {$category->nm_categoria}");

        $this->logRepository->insertLog($log['id_log'],$log["nm_origin"],$logData);

        return $this->redirect("categoriesList");

    }

    public function delete($data)
    {
        $logData["action"] = "categories/delete";
        $logData["dados"] = $data;

        $category = $this->getCategory($data['id_categoria']);

        $data['dt_updated'] = date("Y-m-d H:i:s");
        $data['st_registro'] = 0;

        foreach($data as $k => $v){
            $category->$k = $v;
        }
        $category->save();

        $log["nm_origin"] = "CategoriesRespository";
        $log["id_log"] = $this->uniqueGenerator->init("Log for delete category: {$category->nm_categoria}");

        $this->logRepository->insertLog($log['id_log'],$log["nm_origin"],$logData);

        return $this->redirect("categoriesList");
    }

}