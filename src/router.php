<?php

use CoffeeCode\Router\Router;

$router = new Router(URL_BASE);

$router->namespace(ROOT_NAMESPACE);

$router->group(null);
    $router->get("/","Controllers\Landing:init","landing");
    $router->get("/bot","Controllers\BotMigrate:init");

$router->group("products");
    $router->get("/","Controllers\Products:init","productsList");
    $router->get("/new","Controllers\Products:new");
    $router->post("/add","Controllers\ProductsRepository:add");
    $router->get("/edit/{id_produto}","Controllers\Products:edit");
    $router->post("/edit/{id_produto}","Controllers\ProductsRepository:update");
    $router->get("/delete/{id_produto}","Controllers\Products:delete");
    $router->post("/delete/{id_produto}","Controllers\ProductsRepository:delete");

$router->group("categories");
    $router->get("/","Controllers\Categories:init","categoriesList");
    $router->get("/new","Controllers\Categories:new");
    $router->post("/add","Controllers\CategoriesRepository:add");
    $router->get("/edit/{id_categoria}","Controllers\Categories:edit");
    $router->post("/edit/{id_categoria}","Controllers\CategoriesRepository:update");
    $router->get("/delete/{id_categoria}","Controllers\Categories:delete");
    $router->post("/delete/{id_categoria}","Controllers\CategoriesRepository:delete");


/**
 * Dispatcher
 * execute the called route
 */
$router->dispatch();